<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class AjaxUploadController extends Controller
{
    function index()
    {
     return view('ajax_upload');
    }

    function action(Request $request)
    {
     $validation = Validator::make($request->all(), [
      // 'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
      'caption' => 'required',
     ]);
     if($validation->passes())
     {
	      $image = $request->file('select_file');
	      $caption = request('caption');
	      $images = request('select_file');
	      // dd(count(request('select_file')));
	      $uploaded_image = array();
	      foreach($images as $image){
	      	$new_name = rand() . '.' . $image->getClientOriginalExtension();
	      $image->move(public_path('images'), $new_name);

	      auth()->user()->posts()->create([
    		'caption'=> $caption,
    		'image' => "/images/$new_name",
    		]);
	      	array_push($uploaded_image, '<img src="../images/'.$new_name.'" class="img-thumbnail" width="300" />');
	      	 
	      }
	      // dd($uploaded_image);
	      return response()->json([
	       'message'   => 'Image Upload Successfully',
	       'uploaded_image' => $uploaded_image,
	       'class_name'  => 'alert-success'
	      ]);

      	  
     }
     else
     {
      return response()->json([
       'message'   => $validation->errors()->all(),
       'uploaded_image' => '',
       'class_name'  => 'alert-danger'
      ]);
     }
    }
}
?>
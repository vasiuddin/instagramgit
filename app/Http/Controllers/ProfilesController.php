<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;

class ProfilesController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user)
    {
    	$user = User::findOrFail($user);
    	// dd($user);
        return view('profiles.index', [
        	'user' => $user,
        	]);
    }

    public function create(\App\User $user)
    {
    	// dd($user); 
    	return view('profiles.create', compact('user'));

    	// $data = request()->validate([
    	// 	'title' => 'required',
    	// 	'description' => 'required',
    	// 	'url' => "url",
    	// 	'image' => "",

    	// 	]);

    	// $user->profile->create($data);
    	// return redirect("/profile/{$user->id}");
    	// dd($user->profile);
    }

    public function save(\App\User $user)
    {
    	$user = auth()->user();
    	

    	$data = request()->validate([
    		// 'user_id' => 'required',
    		'title' => '',
    		'description' => '',
    		'url' => "url",
    		'image' => "",

    		]);
    	$data['user_id'] = $user->id;
    	$user->profile = new \App\Profile();
    	// dd($data);

    	$user->profile->create($data);
    	// dd($profile);
    	return redirect("/profile/{$user->id}");
    	// dd($user->profile);
    }

    public function edit(\App\User $user)
    {
    	// $this->authorize('update', $user->profile);
    	return view('profiles.edit', compact('user'));
    }

    public function update(\App\User $user)
    {

    	$data = request()->validate([
    		'title' => 'required',
    		'description' => 'required',
    		'url' => "url",
    		'image' => "",

    		]);

    	$user->profile->update($data);
    	return redirect("/profile/{$user->id}");
    	// dd($user->profile);
    }
}

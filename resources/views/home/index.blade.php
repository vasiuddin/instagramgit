@extends('layouts.app')

@section('content')
<div class="container gallery-container">

    <h1 style="text-align: center;">All User Posts</h1>

    <!-- <p class="page-description text-center">All User Posts</p> -->
    
    <div class="tz-gallery">

        <div class="row">
             @foreach($posts as $post)
             <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <a class="lightbox" href="../public{{ $post->image }}">
                        <img src="../public{{ $post->image }}" alt="Park" style="height:300px">
                    </a>
                    <div class="caption">
                        <h3>{{ $post->user->username }}</h3>
                        <p>{{ $post->caption }}</p>
                    </div>
                </div>
            </div>
            @endforeach

        </div>        

    </div>

</div>
@endsection

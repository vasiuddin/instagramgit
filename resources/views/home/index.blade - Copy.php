@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12" style="padding:35px">
            <h1>This is home page where you call see posts from all users</h1>
            <!-- <img src="https://instagram.fdel1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/71823063_1536327916520857_4179193557728362496_n.jpg?_nc_ht=instagram.fdel1-2.fna.fbcdn.net&_nc_ohc=sC1qNPAu7nsAX-gW2yM&oh=0061f17d332c541d81a4591c3a02123d&oe=5EC7E4A6" style="border-radius:50%"> -->
        </div>
        <!-- <a href="{{url('/p/create')}}" >Add new Post</a> -->
    </div>
    <div class="row">
        @foreach($posts as $post)
        <!-- <div class="col-md-3" style="padding:5px margin:5px" > -->
        <div class="redman-gallery" data-redman-totalCount="17" data-redman-countPerPage="6" data-redman-countPerRow="3" data-redman-startHeight="250" data-redman-theme="dark">

            <img src="../public{{ $post->image }}" style="height:300px ">
            <!-- <b>{{ $post->user->username }}</b> -->
        </div>
        @endforeach

</div>
<div class="container gallery-container">

    <h1>Bootstrap 3 Gallery</h1>

    <p class="page-description text-center">Grid Layout With Zoom Effect</p>
    
    <div class="tz-gallery">

        <div class="row">
            @foreach($posts as $post)
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="../public{{ $post->image }}">
                    <img src="../public{{ $post->image }}" alt="Park">
                </a>
            </div>
            @endforeach
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="../images/bridge.jpg">
                    <img src="../images/bridge.jpg" alt="Bridge">
                </a>
            </div>
            <div class="col-sm-12 col-md-4">
                <a class="lightbox" href="../images/tunnel.jpg">
                    <img src="../images/tunnel.jpg" alt="Tunnel">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="../images/coast.jpg">
                    <img src="../images/coast.jpg" alt="Coast">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="../images/rails.jpg">
                    <img src="../images/rails.jpg" alt="Rails">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="../images/traffic.jpg">
                    <img src="../images/traffic.jpg" alt="Traffic">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="../images/rocks.jpg">
                    <img src="../images/rocks.jpg" alt="Rocks">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="../images/benches.jpg">
                    <img src="../images/benches.jpg" alt="Benches">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="../images/sky.jpg">
                    <img src="../images/sky.jpg" alt="Sky">
                </a>
            </div>
        </div>

    </div>

</div>
@endsection

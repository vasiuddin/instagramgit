@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row">
        <h1>Add New Post</h1>
    </div>
    <form action="{{ route('postStore') }}" enctype="multipart/form-data" method = "post">
        {{ csrf_field() }}
    <div class="row">
        <div class = "col-md-8">
            <div class="form-group{{ $errors->has('caption') ? ' has-error' : '' }}">
                <label for="caption" class="col-md-4 control-label">Post Caption</label>

                <div class="col-md-8">
                    <input id="caption" type="text" class="form-control" name="caption" value="{{ old('caption') }}" required autofocus>

                    @if ($errors->has('caption'))
                        <span class="help-block">
                            <strong>{{ $errors->first('caption') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class = "col-md-8">
            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                <label for="image" class="col-md-4 control-label">Post Image</label>
                <input type="file" name="image[]" class="form-control" required="required" multiple="multiple" />
                @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class = "col-md-8">
                <button class="btn btn-primary">Add New Post</button>
        </div>
    </div>
</form>
</div> -->
<!DOCTYPE html>
<html>
 <head>
  <title>Add New Post</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 </head>
 <body>
  <br />
  <div class="container">
   <h3 align="center">Add New Post</h3>
   <br />
   <div class="alert" id="message" style="display: none"></div>
   <form method="post" id="upload_form" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
     <table class="table">
      <tr>
       <td width="40%" align="left"><label>Caption</label></td>
        <td width="60%"><input id="caption" type="text" class="form-control" name="caption" value="{{ old('caption') }}" required autofocus></td>
      </tr>
      <tr>
       <td width="40%" align="left"><label>Select File for Upload</label></td>
       <td width="60"><input type="file" name="select_file[]" id="select_file" multiple="multiple" /><span class="text-muted">jpg, png, gif</span></td>
      </tr>
      <tr>
       <td width="40%" align="right"></td>
       <td width="30%" align="left"><input type="submit" name="upload" id="upload" class="btn btn-primary" value="Upload"></td>
       <td width="30%" align="left"></td>
      </tr>
     </table>
    </div>
   </form>
   <br />
   <span id="uploaded_image"></span>
  </div>
 </body>
</html>

<script>
$(document).ready(function(){

 $('#upload_form').on('submit', function(event){
  event.preventDefault();
  $.ajax({
   url:"{{ route('ajaxupload.action') }}",
   method:"POST",
   data:new FormData(this),
   dataType:'JSON',
   contentType: false,
   cache: false,
   processData: false,
   success:function(data)
   {
    document.getElementById("upload_form").reset();
    $('#message').css('display', 'block');
    $('#message').html(data.message);
    $('#message').addClass(data.class_name);
    $.each(data.uploaded_image, function(k, v) {
        $('#uploaded_image').html(data.uploaded_image);
    });
    
   }
  })
 });

});
</script>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3" style="padding:35px">
            <img src="https://instagram.fdel1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/71823063_1536327916520857_4179193557728362496_n.jpg?_nc_ht=instagram.fdel1-2.fna.fbcdn.net&_nc_ohc=sC1qNPAu7nsAX-gW2yM&oh=0061f17d332c541d81a4591c3a02123d&oe=5EC7E4A6" style="border-radius:50%">
        </div>
        <div class="col-md-9" style="padding:35px">
            <form action="../{{ $user->id}}" enctype="multipart/form-data" method = "post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                <div class="row">
                    <div class = "col-md-8">
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title</label>

                            <div class="col-md-8">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') ?? $user->profile->title ?? ''}}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                &nbsp;
                <div class="row">
                    <div class = "col-md-8">
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description</label>

                            <div class="col-md-8">
                                <input id="description" type="text" class="form-control" name="description" value="{{ old('description') ?? $user->profile->description ?? '' }}" required autofocus>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                &nbsp;
                <div class="row">
                    <div class = "col-md-8">
                        <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                            <label for="url" class="col-md-4 control-label">Url</label>

                            <div class="col-md-8">
                                <input id="url" type="text" class="form-control" name="url" value="{{ old('url')?? $user->profile->url ?? '' }}" required autofocus>

                                @if ($errors->has('url'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class = "col-md-8">
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Post Image</label>
                            <input type="file" name="image" class="form-control" required="required" />
                            @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class = "col-md-8">
                            <button class="btn btn-primary">Save Profile</button>
                    </div>
                </div>
            </form> 
        </div>
    </div>

   
</div>
@endsection

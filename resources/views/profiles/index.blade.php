@extends('layouts.app')

@section('content')
<div class="container gallery-container">
    <div class="row">
        <div class="col-md-3" style="padding:35px">
            <img src="https://instagram.fdel1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/71823063_1536327916520857_4179193557728362496_n.jpg?_nc_ht=instagram.fdel1-2.fna.fbcdn.net&_nc_ohc=sC1qNPAu7nsAX-gW2yM&oh=0061f17d332c541d81a4591c3a02123d&oe=5EC7E4A6" style="border-radius:50%">
        </div>
        <div class="col-md-9" style="padding:35px">
            <div>
                <b style="font-size:50px">{{ $user->username }}</b>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="../p/create" >Add new Post</a><br>
                @if($user->profile)
                <a href="../profile/{{ $user->id}}/edit" >Edit Profile</a>
                @else
                <a href="../profile/create" >Create Profile</a>
                @endif
            </div>
            <div class="d-flex">
                <div><strong>{{count($user->posts)}}</strong>  posts  &nbsp;&nbsp;&nbsp; <strong>109k</strong> followers &nbsp;&nbsp;&nbsp; <strong>211</strong> following</div>
            </div>
            <br>
            <div><b>{{ $user->profile->title ?? 'N/A' }}</b></div>
            <div>{{ $user->profile->description ?? 'N/A'}}</div>
            <div><a href="{{ $user->profile->url ?? ''}}">{{ $user->profile->url ?? 'N/A' }}</a></div>
        </div>
    </div>

    <h1 style="text-align: center;">My Posts</h1>

    <!-- <p class="page-description text-center">All User Posts</p> -->
    
    <div class="tz-gallery">

        <div class="row">
             @foreach($user->posts as $post)
             <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <a class="lightbox" href="../{{ $post->image }}">
                        <img src="../{{ $post->image }}" alt="Park" style="height:300px">
                    </a>
                    <div class="caption">
                        <h3>{{ $post->user->username }}</h3>
                        <p>{{ $post->caption }}</p>
                    </div>
                </div>
            </div>
            @endforeach

        </div>        

    </div>

</div>
@endsection

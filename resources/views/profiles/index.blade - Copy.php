@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3" style="padding:35px">
            <img src="https://instagram.fdel1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/71823063_1536327916520857_4179193557728362496_n.jpg?_nc_ht=instagram.fdel1-2.fna.fbcdn.net&_nc_ohc=sC1qNPAu7nsAX-gW2yM&oh=0061f17d332c541d81a4591c3a02123d&oe=5EC7E4A6" style="border-radius:50%">
        </div>
        <div class="col-md-9" style="padding:35px">
            <div>
                <b style="font-size:50px">{{ $user->username }}</b>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="../p/create" >Add new Post</a><br>
                @if($user->profile)
                <a href="../profile/{{ $user->id}}/edit" >Edit Profile</a>
                @else
                <a href="../profile/create" >Create Profile</a>
                @endif
            </div>
            <div class="d-flex">
                <div><strong>{{count($user->posts)}}</strong>  posts  &nbsp;&nbsp;&nbsp; <strong>109k</strong> followers &nbsp;&nbsp;&nbsp; <strong>211</strong> following</div>
            </div>
            <br>
            <div><b>{{ $user->profile->title ?? 'N/A' }}</b></div>
            <div>{{ $user->profile->description ?? 'N/A'}}</div>
            <div><a href="{{ $user->profile->url ?? ''}}">{{ $user->profile->url ?? 'N/A' }}</a></div>
        </div>
    </div>
    <div class="row">
        @foreach($user->posts as $post)
        <div class="col-md-4" style="padding:5px margin:5px" >
            <img src="..{{ $post->image }}" style="height:300px ">
        </div>
        @endforeach
        <!-- <div class="col-md-4" style="padding:5px margin:5px" >
            <img src="https://blog.hubspot.com/hubfs/homepage-web-design.jpg" style="height:300px">
        </div>
        <div class="col-md-4" style="padding:5px margin:5px" >
            <img src="https://www.bmmagazine.co.uk/wp-content/uploads/2016/08/shutterstock_252720676-e1470403262619.jpg" style="height:300px">
        </div> -->
       <!-- <div class="col-md-4">hello</div>
       <div class="col-md-4"></div> -->
    </div>&nbsp
    <!-- <div class="row">
        <div class="col-md-4" style="padding:5px margin:5px" >
            <img src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2018/03/01113506/image48.jpg" style="height:300px ">
        </div>
        <div class="col-md-4" style="padding:5px margin:5px" >
            <img src="https://blog.hubspot.com/hubfs/homepage-web-design.jpg" style="height:300px">
        </div>
        <div class="col-md-4" style="padding:5px margin:5px" >
            <img src="https://www.bmmagazine.co.uk/wp-content/uploads/2016/08/shutterstock_252720676-e1470403262619.jpg" style="height:300px">
        </div>
       <!-- <div class="col-md-4">hello</div>
       <div class="col-md-4"></div> -->
    <!-- </div>&nbsp
    <div class="row">
        <div class="col-md-4" style="padding:5px margin:5px" >
            <img src="https://s3.amazonaws.com/ceblog/wp-content/uploads/2018/03/01113506/image48.jpg" style="height:300px ">
        </div>
        <div class="col-md-4" style="padding:5px margin:5px" >
            <img src="https://blog.hubspot.com/hubfs/homepage-web-design.jpg" style="height:300px">
        </div>
        <div class="col-md-4" style="padding:5px margin:5px" >
            <img src="https://www.bmmagazine.co.uk/wp-content/uploads/2016/08/shutterstock_252720676-e1470403262619.jpg" style="height:300px">
        </div>
        <div class="col-md-4">hello</div>
       <div class="col-md-4"></div> 
    </div> -->
</div>
@endsection
